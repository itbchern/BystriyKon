package com.company;

import java.util.ArrayList;

/**
 * Created by Alexander on 22.08.2017.
 */
public class Mover {
    public int[] position;
    public int numberOfMoves;
    public ArrayList<Integer[]> moves;

    public Mover(int[] position, int numberOfMoves) {
        moves = new ArrayList<>();
        this.position = position;
        this.numberOfMoves = numberOfMoves;
    }

    public Mover(Mover mover) {
        moves = new ArrayList<>(mover.moves);
        this.position = new int[2];
        this.position[0] = mover.position[0];
        this.position[1] = mover.position[1];
        this.numberOfMoves = mover.numberOfMoves;
    }

    public Mover getMove(boolean isUp, boolean isRight, boolean isFirstLong) {
        if (isFirstLong) {
            if (isUp) {
                position[1] += 2;
                if (isRight)
                    position[0] += 1;
                else
                    position[0] -= 1;
            } else {
                position[1] -= 2;
                if (isRight)
                    position[0] += 1;
                else
                    position[0] -= 1;
            }

        } else {
            if (isUp) {
                position[1] += 1;
                if (isRight)
                    position[0] += 2;
                else
                    position[0] -= 2;
            } else {
                position[1] -= 1;
                if (isRight)
                    position[0] += 2;
                else
                    position[0] -= 2;
            }
        }

        if (position[0] < 1 || position[0] > 8 || position[1] < 1 || position[1] > 8)
            numberOfMoves = 100;
        else {
            numberOfMoves++;
            moves.add(new Integer[] {position[0], position[1]});
        }

        return this;
    }

    public Mover compare(Mover comparingMover) {
        if (this.numberOfMoves <= comparingMover.numberOfMoves)
            return this;
        else
            return comparingMover;
    }
}