package com.company;

/**
 * Created by Alexander on 22.08.2017.
 */
public class Drawer {
    public static void drawCell(boolean isFilled) {
        char ch;
        if (isFilled)
            ch = 8416;

        else
            ch = 8414;

        System.out.print(ch);
    }

    public static void drawGrid(int x, int y) {
        for (int i = 1; i < 9; i++) {
            for (int j = 1; j < 9; j++) {
                if (i == x && j == y)
                    Drawer.drawCell(true);
                else
                    Drawer.drawCell(false);
            }

            System.out.println();
        }
    }
}
