package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int startX = 1;
        int startY = 1;

        int endX = 8;
        int endY = 8;

        Mover mover = new Mover(new int[] {startX, startY}, 0);
        Mover bestMover = bestMoves(mover, endX, endY);

        System.out.println("Best moves: " + bestMover.numberOfMoves);

        System.out.println();
        Drawer.drawGrid(startX, startY);

        for (Integer[] move: bestMover.moves) {
            System.out.println();
            System.out.println(move[0] + " - " + move[1]);
            Drawer.drawGrid(move[0], move[1]);
        }
    }

    public static Mover bestMoves(Mover mover, int endX, int endY) {
        int moveNumber = 10;

        if (mover.position[0] == endX && mover.position[1] == endY)
            return mover;

        if (mover.numberOfMoves < moveNumber) {
            Mover fff = new Mover(mover).getMove(false, false, false);
            Mover fft = new Mover(mover).getMove(false, false, true);
            Mover ftf = new Mover(mover).getMove(false, true, false);
            Mover tff = new Mover(mover).getMove(true, false, false);
            Mover ftt = new Mover(mover).getMove(false, true, true);
            Mover tft = new Mover(mover).getMove(true, false, true);
            Mover ttf = new Mover(mover).getMove(true, true, false);
            Mover ttt = new Mover(mover).getMove(true, true, true);


            Mover min = bestMoves(fff, endX, endY);

            if (fft.numberOfMoves < moveNumber)
                min = min.compare(bestMoves(fft, endX, endY));
            if (ftf.numberOfMoves < moveNumber)
                min = min.compare(bestMoves(ftf, endX, endY));
            if (tff.numberOfMoves < moveNumber)
                min = min.compare(bestMoves(tff, endX, endY));
            if (ftt.numberOfMoves < moveNumber)
                min = min.compare(bestMoves(ftt, endX, endY));
            if (tft.numberOfMoves < moveNumber)
                min = min.compare(bestMoves(tft, endX, endY));
            if (ttf.numberOfMoves < moveNumber)
                min = min.compare(bestMoves(ttf, endX, endY));
            if (ttt.numberOfMoves < moveNumber)
                min = min.compare(bestMoves(ttt, endX, endY));


            return min;
        }

        return mover;
    }





}
